message("pri file being processed: $$PWD")

HEADERS += \
    $$PWD/dynamicdigraphalgorithm.h \
    $$PWD/dynamicweighteddigraphalgorithm.h \
    $$PWD/staticalgorithmwrapper.h

SOURCES += \
    $$PWD/dynamicdigraphalgorithm.cpp \
    $$PWD/dynamicweighteddigraphalgorithm.cpp \
    $$PWD/staticalgorithmwrapper.cpp
